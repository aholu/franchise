/**
 * Created by aholu on 05/03/18.
 */

import React, { Component } from 'react';
import { Redirect, withRouter } from "react-router-dom";
import Form from './form.js';
import { createAppeal } from './action.js';

export default class Create extends Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleSubmit(data) {
        createAppeal(data);
        window.location = '/'
    }

    render() {
        return (
            <div className="container col-sm-10">
                <div className="header clearfix">
                    <h3 className="text-muted">LOKUMCU BABA</h3>
                </div>
                <div className="jumbotron">
                    <Form onSubmit={this.handleSubmit}></Form>
                </div>
            </div>
        );
    }
}
