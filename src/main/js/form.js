/**
 * Created by aholu on 05/03/18.
 */
import React, { Component } from 'react';


export default class Form extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            surname: '',
            identity: '',
            address: '',
            phone: '',
            mail: '',
            birthDate: '',
            retail: '',
            chooseReason: '',
            storeAddress: '',
            budget: '',
            extra: ''
        };
        this.handleUserInput = this.handleUserInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleUserInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value});
    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.onSubmit(this.state);
    }

    render() {
        return (
            <form className="form-horizontal" onSubmit={this.handleSubmit}>
                <div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required">ISIM : </label>
                        <div className="col-sm-10">
                            <input type="text" required className="form-control" name="name"
                                   value={this.state.name}
                                   onChange={this.handleUserInput}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required">SOYISIM : </label>
                        <div className="col-sm-10">
                            <input type="text" required className="form-control" name="surname"
                                   value={this.state.surname}
                                   onChange={this.handleUserInput}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required">TC KIMLIK NO : </label>
                        <div className="col-sm-10">
                            <input type="number" required className="form-control" name="identity" max={99999999999} min={10000000000}
                                   value={this.state.identity}
                                   onChange={this.handleUserInput}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required">ADRES : </label>
                        <div className="col-sm-10">
                            <input type="text" required className="form-control" name="address"
                                   value={this.state.address}
                                   onChange={this.handleUserInput}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required">TELEFON : </label>
                        <div className="col-sm-10">
                            <input type="number" required className="form-control" name="phone"
                                   placeholder="5011234567"
                                   value={this.state.phone}
                                   onChange={this.handleUserInput}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required">E-POSTA : </label>
                        <div className="col-sm-10">
                            <input type="email" required className="form-control" name="mail"
                                   value={this.state.mail}
                                   onChange={this.handleUserInput}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required">DOGUM TARIHI : </label>
                        <div className="col-sm-10">
                            <input type="date" required className="form-control" name="birthDate"
                                   value={this.state.birthDate}
                                   onChange={this.handleUserInput}/>
                        </div>
                    </div>
                    <fieldset className="form-group">
                        <label className="col-sm-2 control-label">PERAKENDE TİCARETİ İLE UĞRAŞTINIZ MI? : </label>
                        <div className="form-check">
                            <label className="form-check-label">
                                <input type="radio" className="form-check-input" name="retail" value="true" defaultChecked/>
                                    EVET
                            </label>
                        </div>
                        <div className="form-check">
                            <label className="form-check-label">
                                <input type="radio" className="form-check-input" name="retail" value="false"/>
                                    HAYIR
                            </label>
                        </div>
                    </fieldset>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required">LOKUMCU BABA’YI TERCİH ETMENİZİN SEBEBİ NEDİR? : </label>
                        <div className="col-sm-10">
                            <input type="textArea" className="form-control" name="chooseReason"
                                   value={this.state.chooseReason}
                                   onChange={this.handleUserInput}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required">HANGİ İL/İLÇE/SEMT İÇİN LOKUMCU BABA İŞLETMECİLİĞİ
                            DÜŞÜNÜYORSUNUZ? : </label>
                        <div className="col-sm-10">
                            <input type="text" required className="form-control" name="storeAddress"
                                   value={this.state.storeAddress}
                                   onChange={this.handleUserInput}/>
                        </div>
                    </div>
                    <div className="form-group ">
                        <label className="col-sm-2 control-label required">YATIRIM MİKTARINIZ NEDİR? : </label>
                        <div className="input-group col-sm-10">
                            <input type="number" required className="form-control" name="budget"
                                   value={this.state.budget}
                                   onChange={this.handleUserInput}/>
                            <span className="input-group-addon">TL</span>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required">EKLEMEK İSTEDİKLERİNİZ : </label>
                        <div className="col-sm-10">
                            <input type="textArea" className="form-control" name="extra"
                                   value={this.state.extra}
                                   onChange={this.handleUserInput}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-2"></div>
                        <div className="col-sm-10">
                            <button type="submit"
                                    className="btn-default btn">
                                BASVUR
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        );
    }

}