import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Route, Switch } from 'react-router-dom';
import Create from './create.js';
import List from './list.js';

class App extends Component {

    render() {
        return (
            <div className="container">
                <div className="header clearfix">
                    <h3 className="text-muted">LOKUMCU BABA</h3>
                </div>
                <div className="jumbotron">
                    <h4>Lokumcu Baba'ya Hosgeldiniz!</h4>
                    <p className="">Franchise basvurusu yapmak icin tiklayiniz</p>
                    <p>
                        <Link to="/appeal/create">
                            <button className="btn btn-lg btn-success">Basvur</button>
                        </Link>
                    </p>
                </div>
            </div>
        );
    }
}

ReactDOM.render(
    <Router>
        <div>
            <Route exact path="/" component={App}/>
            <Route path="/appeal/create" component={Create}/>
        </div>
    </Router>,
    document.getElementById('react')
);


