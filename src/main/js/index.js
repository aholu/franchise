/**
 * Created by aholu on 05/03/18.
 */

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Index extends Component {

    render() {
        return (
            <div className="App">
                <div className='col-sm-10'>
                    <h2>Lokumcu BABA'ya Hosgeldiniz</h2>
                    <Link to="/appeal/create" className="btn btn-lg btn-success">Basvur</Link>
                </div>
            </div>
        );
    }
}
