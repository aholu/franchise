/**
 * Created by aholu on 05/03/18.
 */
import fetch from 'isomorphic-fetch';

export function createAppeal(data) {
    return fetch('http://localhost:8080/api/add', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => {
        return res;
    }).catch(err => err);
}
