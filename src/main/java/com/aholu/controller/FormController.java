package com.aholu.controller;

import com.aholu.model.Form;
import com.aholu.service.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by aholu on 05/03/18.
 */
@RestController
@RequestMapping("/api")
public class FormController {

    @Autowired
    private FormService formService;

    @RequestMapping(path = "/appeals")
    public List<Form> getAllAppeal() {

        return formService.getAllAppeeal();
    }

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public void addAppeal(@RequestBody Form form) {
        formService.add(form);
    }

    @RequestMapping(path = "/delete/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        formService.deleteForm(id);
    }

    @RequestMapping(path = "/get/{id}", method = RequestMethod.GET)
    public Form getAppealById(@PathVariable(value = "id") Long id) {
        return formService.getById(id);
    }
}
