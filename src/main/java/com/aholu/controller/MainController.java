package com.aholu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by aholu on 05/03/18.
 */
@Controller
public class MainController {
    @RequestMapping(value = "/")
    public String index() {
        return "index";
    }

}
