package com.aholu.dao;

import com.aholu.model.Form;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by aholu on 05/03/18.
 */
@Repository
public interface FormDao extends CrudRepository<Form, Long> {
}