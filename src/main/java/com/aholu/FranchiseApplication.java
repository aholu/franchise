package com.aholu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by aholu on 05/03/18.
 */
@SpringBootApplication
@EnableJpaRepositories("com.aholu.dao")
@EntityScan("com.aholu.model")
@ComponentScan(basePackages = "com.aholu")
public class FranchiseApplication {

    public static void main(String[] args) {
        SpringApplication.run(FranchiseApplication.class, args);
    }
}
