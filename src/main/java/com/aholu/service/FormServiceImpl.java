package com.aholu.service;

import com.aholu.dao.FormDao;
import com.aholu.model.Form;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by aholu on 0/03/18.
 */
@Service
public class FormServiceImpl implements FormService {

    @Autowired
    private FormDao formDao;

    @Override
    public void add(Form form) {
        formDao.save(form);
    }

    @Override
    public List<Form> getAllAppeeal() {
        return (List<Form>) formDao.findAll();
    }

    @Override
    public Form getById(Long id) {
        return formDao.findOne(id);
    }

    @Override
    public void deleteForm(Long id) {

        formDao.delete(id);

    }
}

