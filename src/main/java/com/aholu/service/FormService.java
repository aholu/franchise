package com.aholu.service;

import com.aholu.model.Form;

import java.util.List;

/**
 * Created by aholu on 05/03/18.
 */
public interface FormService {

    public void add(Form form);

    public List<Form> getAllAppeeal();

    public Form getById(Long id);

    public void deleteForm(Long id);
}
